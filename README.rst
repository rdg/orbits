orbits
======

orbits is a simple c++ program that allows the creation of generic bodies,
their addition to a system, and the calculation of several properties of the
resulting system.

The total energy and angular momentum can be calculated for any system (in the
centre of mass frame), and for systems with 2 bodies, the orbital period,
eccentricity, semi-major axis and semi latus rectum can also be calculated.

A simple n-body code is also implemented, allowing the evolution of the system
across a specified number of time-steps to be calculated.

Build details
-------------
The numerical evolution can be plotted through the use of the MathGL library
(http://mathgl.sourceforge.net).
A future TODO is to disable MathGL support within the program if it is not
installed, at either compilation or execution stage.

A makefile can be generated using CMake (http://www.cmake.org), calling
``cmake ../src`` from the ``build`` subdirectory.
This makefile requires CMake to be installed in order for ``make`` to be called
and as such the produced binary (linux 64-bit) is included instead.
Alternatively 'hand' compilation can be performed using ``g++ -std=c++11 -Wall
-o orbits ../src/orbit.cpp ../src/body.cpp ../src/system.cpp -lmgl``, called
from the ``build`` subdirectory.

A few Doxygen (http://www.stack.nl/~dimitri/doxygen) comments have been added,
and a Doxyfile present in ``build``.
The ``docs`` subdirectory will be populated with a command ``doxygen Doxyfile``
called within the ``build`` subdirectory, open ``docs/html/index.html`` to
begin viewing.
It may be worth producing the documentation using Sphinx
(http://sphinx.pocoo.org) via Breathe (http://michaeljones.github.com/breathe)
or Robin (https://bitbucket.org/reima/robin/overview), due to its better
appearance, theming and (possible) ease of use.

A file ``orbits.py`` is present in ``src``, and performs the same numerical
evolution and plotting as the c++ code, however for only 2 bodies.

