#include <iostream>
#include <vector>
#include "body.h"
#include "system.h"

void print_vec(std::vector<double> vec){
    std::cout<<vec[0]<<" "<<vec[1]<<" "<<vec[2]<<std::endl;
}

int main() {
   
    // Construct the system and body objects 
    CSystem omicron_persei;
    CBody sun(2e30, "Sun");
    CBody earth(6e24, "Earth");
    CBody venus(4.9e24, "Venus");

    sun.vf_position(std::vector<double> {0, 0, 0});
    sun.vf_velocity(std::vector<double> {0, 0, 0});
    earth.vf_position(std::vector<double> {1.5e11, 0, 0});
    earth.vf_velocity(std::vector<double> {0, 3e4, 0});
    venus.vf_position(std::vector<double> {1.08e11, 0, 0});
    venus.vf_velocity(std::vector<double> {0, 3.5e4, 0});

    // Add bodies to system
    omicron_persei.v_add_body(sun);
    omicron_persei.v_add_body(earth);
    omicron_persei.v_add_body(venus);

    std::cout<<"System bodies:"<<std::endl;
    omicron_persei.v_print_bodies();

    // Analytical calculations (for 2 bodies)
    omicron_persei.vf_to_cm_frame();
    std::cout<<"energy "<<omicron_persei.df_energy()<<std::endl;
    std::cout<<"angmom "<<omicron_persei.df_ang_mom()<<std::endl;
    std::cout<<"a = "<<omicron_persei.df_semi_major_axis()<<std::endl;
    std::cout<<"P = "<<omicron_persei.df_period("years")<<" years"<<std::endl;
    //1yr = 3.15e7 seconds
    std::cout<<"l = "<<omicron_persei.df_semi_latus_rectum()<<std::endl;
    std::cout<<"e = "<<omicron_persei.df_eccentricity()<<std::endl;

    // Numerical calculations
    //                              step   num_steps
    omicron_persei.v_plot_evolution(10000, 20000);

    return 0;
}

