#include <iostream>
#include <algorithm>
#include "body.h"

// Default constructor
CBody::CBody(){
    d_mass = 6e24;
    s_name = "Earth";
    da_position = std::vector<double> {0, 0, 0,};
    da_velocity = std::vector<double> {0, 0, 0,};
    da_acceleration = std::vector<double> {0, 0, 0,};
}

// Constructor with parameters
CBody::CBody(double d_body_mass, std::string s_body_name) {
    d_mass = d_body_mass;
    if (d_body_mass < 0.){
        std::cout<<"Mass of "<<s_body_name
            <<" is invalid, setting to 6e24"<<std::endl;
        d_mass = 6e24;
    }
    s_name = s_body_name;
    da_position = std::vector<double> {0, 0, 0,};
    da_velocity = std::vector<double> {0, 0, 0,};
    da_acceleration = std::vector<double> {0, 0, 0,};
}

// Mass getter (can return in units of kg, M_Earth or M_Sun)
double CBody::df_mass(std::string format){
    std::string format_lower = format;
    std::transform(format_lower.begin(), format_lower.end(),
                   format.begin(), ::tolower                );
    if (format_lower == "m_earth"){ return (d_mass / 5.9736e24); }
    if (format_lower == "m_sun") { return (d_mass / 1.9891e30); }
    if (format_lower != "kg") {
        std::cout<<"Unknown unit for mass, returning in kg"<<std::endl;
    }
    return d_mass;
}


// Position getter
void CBody::vf_position(std::vector<double> da_pos){
    if (da_pos.size() == da_position.size()){
        da_position = da_pos;
    }
    else {
        // bad things happen
    }
}

// Velocity getter
void CBody::vf_velocity(std::vector<double> da_vel){
    if (da_vel.size() == da_velocity.size()){
        da_velocity = da_vel;
    }
    else {
        // bad things happen
    }
}

// Acceleration getter
void CBody::vf_acceleration(std::vector<double> da_acc){
    if (da_acc.size() == da_acceleration.size()){
        da_acceleration = da_acc;
    }
    else {
        // bad things happen
    }
}

