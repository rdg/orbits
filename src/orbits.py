import matplotlib.pyplot as plt
import numpy as np
import scipy.constants

G = scipy.constants.G

mass_1 = 2e30
mass_2 = 6e24
time = 0
time_step = 10000
num_steps = 20000

position_1 = np.array([0., 0., 0.])
velocity_1 = np.array([0., 0., 0.])
position_2 = np.array([1.5e11, 0., 0.])
velocity_2 = np.array([0., 4e4, 0.])
position_1_history = np.zeros((num_steps, 3))
position_2_history = np.zeros((num_steps, 3))

for step in range(num_steps):
    time += time_step

    displacement = position_2 - position_1
    acceleration_1 = (G * mass_2 / np.sum((displacement) ** 2) *
                      displacement / np.linalg.norm(displacement))
    #print(G * mass_2 / np.sum((displacement) ** 2),
    #      displacement / np.linalg.norm(displacement))
    acceleration_2 = (-G * mass_1 / np.sum((displacement) ** 2) *
                      displacement / np.linalg.norm(displacement))
    position_1 = (position_1 + velocity_1 * time_step +
                  0.5 * acceleration_1 * time_step ** 2)
#    print(np.linalg.norm(displacement), acceleration_1, position_1)
    position_2 = (position_2 + velocity_2 * time_step +
                  0.5 * acceleration_2 * time_step ** 2)
    velocity_1 += acceleration_1 * time_step
    velocity_2 += acceleration_2 * time_step
    position_1_history[step] = position_1
    position_2_history[step] = position_2

print(position_2)

plt.plot(position_1_history[:, 0], position_1_history[:, 1])
plt.plot(position_2_history[:, 0], position_2_history[:, 1])
plt.show()

