#ifndef SYSTEM_H
#define SYSTEM_H

#include <iostream>
#include <string>
#include "body.h"

/**
   A system of CBody objects
 */

class CSystem {

    public:
        CSystem(){}
        CSystem(std::vector<CBody> oa_initbodies) {
            oa_bodies = oa_initbodies;
        }
        ~CSystem(){}

        // Body management
        void v_add_body(CBody o_body);
        void v_print_bodies();

        // CM frame
        // Getters
        std::vector<double> daf_pos_cm();
        std::vector<double> daf_vel_cm();
        // Conversion to CM frame (could add a conversion back)
        void vf_to_cm_frame();

        // Analytical calculations (only 2 bodies)
        /**
           Return the total energy of the system (kinetic + potential).
           
           @return double
                   The total energy of the system in joules.
         */
        double df_energy();
        double df_ang_mom();
        double df_semi_major_axis();
        double df_semi_latus_rectum();
        double df_eccentricity();
        
        /**
           Return the period of a 2 body system in units of s, days or years

           @param format
                  Units to return mass in, options are:
                  "kg" (default)
                  "M_Earth": 5.9736e24 kg
                  "M_Sun": 1.9891e30 kg

           @return double
                   The mass of the body in the specified units                  
         */
        double df_period(std::string format = "seconds");

        // Numerical evolution
        void v_num_evolution(double d_time_step = 10000,
                             long l_num_steps = 10000,
                             std::vector<std::vector<std::vector<double>>>*
                                                      pda_pos_history = NULL);
        void v_plot_evolution(double d_time_step = 10000,
                              long l_num_steps = 10000   );
    
    private:
        std::vector<CBody> oa_bodies;
};

#endif

