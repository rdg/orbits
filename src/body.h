#ifndef BODY_H
#define BODY_H

#include <string>
#include <vector>
#include <iostream>

/**
   An object such as a planet
 */

class CBody {

    public:
        CBody();
        
        /**
           CBody constructor
           
           Position, velocity and acceleration set to {0, 0, 0}
         
           @param d_body_mass
                  Mass of body (in kg), default: 6e24
            
           @param s_body_name
                  Name of body, default: "Earth"
         */
        CBody(double d_body_mass, std::string s_body_name);

        ~CBody(){}

        // Getters and setters - should probably turn into properties
        std::string sf_name(){ return s_name; };

        /**
           Return the body mass in units of kg, M_Earth or M_Sun

           @param format
                  Units to return mass in, options are:
                  "kg" (default)
                  "M_Earth": 5.9736e24 kg
                  "M_Sun": 1.9891e30 kg

           @return double
                   The mass of the body in the specified units                  
         */
        double df_mass(std::string format = "kg");

        // Position
        std::vector<double> daf_position(){ return da_position; };
        void vf_position(std::vector<double> da_pos);

        // Velocity        
        std::vector<double> daf_velocity(){ return da_velocity; };
        void vf_velocity(std::vector<double> da_vel);
        
        // Acceleration
        std::vector<double> daf_acceleration(){ return da_acceleration; };
        void vf_acceleration(std::vector<double> da_acc);

    private:
        double d_mass;
        std::string s_name;
        std::vector<double> da_position;
        std::vector<double> da_velocity;
        std::vector<double> da_acceleration;
};

#endif

