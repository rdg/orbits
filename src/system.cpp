#include <cmath>
#include <iostream>
#include <mgl2/mgl.h>
#include <mgl2/data.h>

#include "system.h"


const double d_G = 6.67384e-11;


// Vector algebra functions - could use armadillo/eigen or something instead
// Determine the magnitude of a vector
double df_magnitude(std::vector<double> da_vec) {
    double d_mag = 0;
    for (unsigned int i = 0; i < da_vec.size(); i++) {
        d_mag += pow(da_vec[i], 2);
    }
    return pow(d_mag, 0.5);
}

// Vector multiplication
std::vector<double> daf_vec_mult(std::vector<double> da_vec, double d_num) {
    std::vector<double> da_new_vec = da_vec;
    for (unsigned int i = 0; i < da_vec.size(); i++) {
        da_new_vec[i] = d_num * da_vec[i];
    }
    return da_new_vec;
}

// Vector addition
std::vector<double> daf_vec_add(std::vector<double> da_vec1,
                                std::vector<double> da_vec2 ) {
    
    if (da_vec1.size() != da_vec2.size()) {
        std::cout<<"diff size"<<std::endl;
        //something bad
    }
    std::vector<double> da_tot (da_vec1.size(), 0);
    for (unsigned int i = 0; i < da_vec1.size(); i++) {
        da_tot[i] = da_vec1[i] + da_vec2[i];
    }
    return da_tot;
}

// Vector multiplication
std::vector<double> daf_vec_sub(std::vector<double> da_vec1,
                                std::vector<double> da_vec2 ) {
   
    std::vector<double> da_vec2_copy = da_vec2;
    for (unsigned int i = 0; i < da_vec2.size(); i++) {
        da_vec2_copy[i] = -1 * da_vec2[i];
    }
    return daf_vec_add(da_vec1, da_vec2_copy);
}

// Vector cross product
std::vector<double> daf_vec_cross(std::vector<double> da_vec1,
                                 std::vector<double> da_vec2 ) {

    if (da_vec1.size() != 3 && da_vec2.size() != 3) {
        //something bad
    }
    return std::vector<double> {da_vec1[1] * da_vec2[2] -
                                da_vec1[2] * da_vec2[1],
                                da_vec1[2] * da_vec2[0] -
                                da_vec1[0] * da_vec2[2],
                                da_vec1[0] * da_vec2[1] -
                                da_vec1[1] * da_vec2[0]  };
}

// Magnitude of position vector between two bodies
double df_distance(CBody o_body1, CBody o_body2) {
    std::vector<double> da_displacement = 
                                    daf_vec_sub(o_body1.daf_position(),
                                                o_body2.daf_position() );
    return df_magnitude(da_displacement);
}

// Body management
// Add a body to the system
void CSystem::v_add_body(CBody o_body) {
    for (unsigned int i = 0; i < oa_bodies.size(); i++) {
        if (oa_bodies[i].sf_name() == o_body.sf_name()){
            std::cout<<"A body with the name "<<o_body.sf_name()
                <<" already exists in this system, please rename and try again"
                <<std::endl;
            return;
        }
        if (df_distance(oa_bodies[i], o_body) < 1) {
            std::cout<<"This body is less than 1 metre from another,"
                <<" please specify another position and try again"<<std::endl;
        }
    }
    oa_bodies.push_back(o_body);
}

// Print the names and masses of all bodies in the system
void CSystem::v_print_bodies() {
    std::cout<<"Bodies in system:"<<std::endl;
    for(unsigned int i = 0; i < oa_bodies.size(); i++){
        std::cout<<"Name: "<<oa_bodies[i].sf_name()
        <<"   Mass: "<<oa_bodies[i].df_mass()
        <<"kg"<<std::endl;
    }
}

// CM frame
// Return the position of the centre of mass of the system
std::vector<double> CSystem::daf_pos_cm() {
    std::vector<double> da_pos_cm (3, 0);
    double d_total_mass = 0;
    for (unsigned int i = 0; i < oa_bodies.size(); i++) {
        d_total_mass += oa_bodies[i].df_mass();
        da_pos_cm = daf_vec_add(da_pos_cm,
                                daf_vec_mult(oa_bodies[i].daf_position(),
                                             oa_bodies[i].df_mass())     );
    }
    return daf_vec_mult(da_pos_cm, 1 / d_total_mass);
}

// Return the velocity of the centre of mass of the system
std::vector<double> CSystem::daf_vel_cm() {
    std::vector<double> da_vel_cm (3, 0);
    double d_total_mass = 0;
    for (unsigned int i = 0; i < oa_bodies.size(); i++) {
        d_total_mass += oa_bodies[i].df_mass();
        da_vel_cm = daf_vec_add(da_vel_cm,
                                daf_vec_mult(oa_bodies[i].daf_velocity(),
                                             oa_bodies[i].df_mass())     );
    }
    return daf_vec_mult(da_vel_cm, 1 / d_total_mass);
}

// Change coordinate systems to place the centre of mass stationary at 0,0,0
void CSystem::vf_to_cm_frame() {
    std::vector<double> da_pos_cm = daf_pos_cm();
    std::vector<double> da_vel_cm = daf_vel_cm();
    for (unsigned int i = 0; i < oa_bodies.size(); i++) {
        oa_bodies[i].vf_position(daf_vec_sub(oa_bodies[i].daf_position(),
                                             da_pos_cm));
        oa_bodies[i].vf_velocity(daf_vec_sub(oa_bodies[i].daf_velocity(),
                                             da_vel_cm));
    }
}


// Analytical calculations
// Return the total energy of the system
double CSystem::df_energy() {
    double d_energy = 0;

    // Kinetic energy
    for (unsigned int i = 0; i < oa_bodies.size(); i++) {
        d_energy += 0.5 * oa_bodies[i].df_mass() *
                    pow(df_magnitude(oa_bodies[i].daf_velocity()), 2);
    }
    // Gravitational potential energy
    for (int i = (oa_bodies.size() - 1); i > -1; i-- ) {
        for (int j = 0; j < i; j++) {
                d_energy -= (d_G * oa_bodies[i].df_mass() *
                             oa_bodies[j].df_mass() /
                             df_distance(oa_bodies[i], oa_bodies[j]));
        }
    }
    return d_energy;
}

// Return the magnitude of the angular momentum of the system
double CSystem::df_ang_mom() {
    double d_ang_mom = 0;
    for (unsigned int i = 0; i < oa_bodies.size(); i++) {
        d_ang_mom += (oa_bodies[i].df_mass() *
                      df_magnitude(daf_vec_cross(
                              daf_vec_sub(oa_bodies[i].daf_position(),
                                          daf_pos_cm() ),
                              daf_vec_sub(oa_bodies[i].daf_velocity(),
                                          daf_vel_cm() ) )));
    }
    return d_ang_mom;
}

// For a 2 body system, return the semi-major axis of the orbit
double CSystem::df_semi_major_axis() {
    if (oa_bodies.size() != 2) {
        std::cout<<"The system semi-major axis can only be calculated for a 2"
           " body system"<<std::endl;
        //exit(1);
        return 0;
    }
    return (d_G * oa_bodies[0].df_mass() * oa_bodies[1].df_mass() * 0.5
            / (-1 * df_energy()));
}

// For a 2 body system, return the semi latus rectum of the orbit
double CSystem::df_semi_latus_rectum() {
    if (oa_bodies.size() != 2) {
        std::cout<<"The system semi latus rectum can only be calculated for a"
           " 2 body system"<<std::endl;
        //exit(1);
        return 0;
    }
    return (pow(df_ang_mom(), 2) *
            (oa_bodies[0].df_mass() + oa_bodies[1].df_mass()) /
            (d_G * pow(oa_bodies[0].df_mass() * oa_bodies[1].df_mass(), 2)));
}

// For a 2 body system, return the eccentricity of the orbit
double CSystem::df_eccentricity() {
    if (oa_bodies.size() != 2) {
        std::cout<<"The system eccentricity can only be calculated for a 2"
           " body system"<<std::endl;
        //exit(1);
        return 0;
    }
    double d_energy = df_energy();
    double d_e = 0;
    if (d_energy > 0) {
        d_e = pow((df_semi_latus_rectum() / df_semi_major_axis()) + 1, 0.5);

    }
    else {
        d_e = pow(1 - (df_semi_latus_rectum() / df_semi_major_axis()), 0.5);
    }
    return d_e;
}

// For a 2 body system, return the period of the orbit
double CSystem::df_period(std::string format) {
    if (oa_bodies.size() != 2) {
        std::cout<<"The system period can only be calculated for a 2 body"
            <<" system"<<std::endl;
        //exit(1);
        return 0;
    }
    double seconds = sqrt(4 * pow(M_PI, 2) * pow(df_semi_major_axis(), 3) /
                          (d_G * (oa_bodies[0].df_mass() +
                                  oa_bodies[1].df_mass()   ))              );
    double divisor = 1;
    if (format == "seconds") {}
    else if (format == "days") {
        divisor = (24 * 3600);
    }
    else if (format == "years") {
        divisor = (365 * 24 * 3600);
    }
    else {
        std::cout<<"Unknown period format, returning in seconds"<<std::endl;
    }
    return seconds / divisor;
}


// Numerical evolution
// Evolve the system over some number of time steps and fill an array with the
// position history, such that it can be plotted
void CSystem::v_num_evolution(double d_time_step, long l_num_steps,
                              std::vector<std::vector<std::vector<double>>>*
                              pda_pos_history){
    double d_time = 0;
    // Cycle through time steps
    for (long step = 0; step < l_num_steps; step++) {
        d_time += d_time_step;
        // Cycle through bodies
        for (unsigned int i = 0; i < oa_bodies.size(); i++) {
            // Acceleration 
            oa_bodies[i].vf_acceleration(std::vector<double> {0, 0, 0});
            for (unsigned int j = 0; j < oa_bodies.size(); j++) {
                if (j != i) {
                    std::vector<double> da_displacement = 
                                    daf_vec_sub(oa_bodies[j].daf_position(),
                                                oa_bodies[i].daf_position() );
                    double d_displacement = df_magnitude(da_displacement);
                    
                    oa_bodies[i].vf_acceleration(
                        daf_vec_add(oa_bodies[i].daf_acceleration(),
                                    daf_vec_mult(da_displacement,
                                                 d_G * oa_bodies[j].df_mass()
                                                 / pow(d_displacement, 3)    )
                                    ));
                }
            }
            // Position
            oa_bodies[i].vf_position(
              daf_vec_add(oa_bodies[i].daf_position(),
                daf_vec_add(daf_vec_mult(oa_bodies[i].daf_velocity(),
                                         d_time_step                 ),
                            daf_vec_mult(oa_bodies[i].daf_acceleration(),
                                         0.5 * pow(d_time_step, 2)       ))));
            // Velocity
            oa_bodies[i].vf_velocity(
                daf_vec_add(oa_bodies[i].daf_velocity(),
                            daf_vec_mult(oa_bodies[i].daf_acceleration(),
                                         d_time_step                     )));
            // Write to position history
            std::vector<double> da_pos_buffer = oa_bodies[i].daf_position();
            for (int j = 0; j < 3; j++) {
                // body, time step, position component
                (*pda_pos_history)[i][step][j] = da_pos_buffer[j];
            }
        }
    }
}

// Evolve the system over some time and plot it using MathGL
void CSystem::v_plot_evolution(double d_time_step, long l_num_steps) {
    // Create history vector to fill
    std::vector<std::vector<std::vector<double>>> da_pos_history 
        (oa_bodies.size(), std::vector<std::vector<double>>
                           (l_num_steps, std::vector<double> (3, 0)));
    // Fill history
    v_num_evolution(d_time_step, l_num_steps, &da_pos_history);

    // Setup plot
    mglGraph gr;
    gr.SetSize(1200, 1200);
    gr.SetRanges(-2e11, 2e11, -2e11, 2e11);
    gr.SetOrigin(0,0);
    gr.Axis();
    gr.Box();
    gr.Grid();
    mglData x(l_num_steps), y(l_num_steps), z(l_num_steps);
    // Body
    for (unsigned int i = 0; i < oa_bodies.size(); i++) {
        // Time step
        for (long j = 0; j < l_num_steps; j++) {
            // Coordinate
            x[j] = da_pos_history[i][j][0];
            y[j] = da_pos_history[i][j][1];
            z[j] = da_pos_history[i][j][2];
        }
        gr.Plot(x, y, z);
    }
    std::cout<<"Saving plot"<<std::endl;
    gr.WritePNG("orbit.png");
    //gr.WriteEPS("orbit.ps");
}
