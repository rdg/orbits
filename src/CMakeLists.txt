# The name of the project
project(orbits)

# Probably not necessary but who knows...
cmake_minimum_required(VERSION 2.6)

# Extra compiler options
add_definitions(-std=c++11 -Wall)

# Specify sources files to build with
set(orbits_sources orbits.cpp body.cpp system.cpp)
# Or use all cpp sources for executable (rather than specifying individually)
#file(GLOB orbit_sources ../src/*.cpp)

# Compile and link
add_executable(orbits ${orbits_sources})
target_link_libraries (orbits mgl)

# In case someone wants to install orbit after making
install(TARGETS orbits RUNTIME DESTINATION bin)
